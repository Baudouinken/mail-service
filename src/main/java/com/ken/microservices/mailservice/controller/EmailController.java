package com.ken.microservices.mailservice.controller;

import com.ken.microservices.mailservice.pojos.Email;
import com.ken.microservices.mailservice.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;

@RestController
@RequestMapping("/mail-service/mail")
@CrossOrigin(origins = "*")
public class EmailController {

	@Autowired
    private MailService mailService;


    @PostMapping("/simple-email")
    public ResponseEntity<String> sendAttachmentEmail(@RequestBody Email mail) throws MessagingException, IOException {
        mailService.sendHtmlMessage(mail);
        return new ResponseEntity<>("mail sent successfully", HttpStatus.OK);
    }

    @PostMapping("/basic-email")
    public ResponseEntity<String> sendBasic(@RequestBody Email mail) {
        mailService.simpleMail(mail);
        return new ResponseEntity<>("mail sent successfully", HttpStatus.OK);
    }
    
    @PostMapping("/mail-attachment")
    public ResponseEntity<String> sendAttachmentEmail(@RequestPart Email mail, @RequestPart MultipartFile content) throws MessagingException, IOException {
        mailService.sendHtmlMessageAttachment(mail, content);
        return new ResponseEntity<>("Attachment mail sent successfully", HttpStatus.OK);
    }
}
