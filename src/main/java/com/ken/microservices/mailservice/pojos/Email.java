package com.ken.microservices.mailservice.pojos;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Map;

@Data
@FieldDefaults(level=AccessLevel.PRIVATE)
public class Email {

    String [] toMail;

    String fromEmail;

    String subject;

    String text;

    String template;
    
    boolean reply;
    
    Map<String, Object> properties;
}