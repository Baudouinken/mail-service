package com.ken.microservices.mailservice.repositories;

import com.ken.microservices.mailservice.models.EmailEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface EmailRepository extends JpaRepository<EmailEntity, UUID> {
	
	EmailEntity findEmailEntityByFromMail(String name);

	EmailEntity findEmailEntityBySubject(String email);

}
