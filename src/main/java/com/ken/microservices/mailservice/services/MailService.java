package com.ken.microservices.mailservice.services;

import com.ken.microservices.mailservice.models.EmailEntity;
import com.ken.microservices.mailservice.pojos.Email;
import com.ken.microservices.mailservice.repositories.EmailRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
@Slf4j
public class MailService {

	@Autowired
	private EmailRepository emailRepository;

	@Autowired
	private JavaMailSender emailSender;

	@Autowired
	private SpringTemplateEngine templateEngine;

	@Value("${template.email.simple-email}")
	private String template_simple_email;

	// simple mail
	public void sendHtmlMessage(Email email) throws MessagingException, IOException {

		MimeMessage message = emailSender.createMimeMessage();

		message.reply(email.isReply());

		MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
				StandardCharsets.UTF_8.name());

		Context context = new Context();

		context.setVariables(email.getProperties());

		helper.setFrom(email.getFromEmail());
		helper.setTo(email.getToMail());
		helper.setSubject(email.getSubject());

		String html = templateEngine.process(email.getTemplate(), context);
		helper.setText(html, true);

		log.info("Sending email: {} with html body: {}", email, html);
		try {
			emailSender.send(message);
			templateEngine.clearTemplateCache();

			saveEmail(email);
		} catch (Exception e) {
			log.error("Error during email sending : %s", e.getMessage());
		}
	}

	// Mail with multipartFile
	public void sendHtmlMessageAttachment(Email email, MultipartFile content) throws MessagingException, IOException {

		MimeMessage message = emailSender.createMimeMessage();

		message.reply(email.isReply());

		MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
				StandardCharsets.UTF_8.name());

		Context context = new Context();

		context.setVariables(email.getProperties());

		helper.setFrom(email.getFromEmail());
		helper.setTo(email.getToMail());
		helper.setSubject(email.getSubject());

		String html = templateEngine.process(email.getTemplate(), context);
		helper.setText(html, true);

		DataSource ds = new ByteArrayDataSource(content.getBytes(), content.getContentType());

		helper.addAttachment(content.getOriginalFilename(), ds);

		log.info("Sending email: {} with html body: {}", email, html);
		try {
			emailSender.send(message);
			templateEngine.clearTemplateCache();

			saveEmail(email);
		} catch (Exception e) {
			log.error("Error during email sending : {}", e.getMessage());
		}
	}

	public void simpleMail(Email email) {
		MimeMessage message=emailSender.createMimeMessage();
		MimeMessageHelper helper= null;
		try {
			helper = new MimeMessageHelper(message,true);
			helper.setTo(email.getToMail());
			helper.setSubject(email.getSubject());
			helper.setText(email.getText());
			emailSender.send(message);
			log.info("Email sent");
		} catch (MessagingException e) {
			log.error("Error during email sending : {}", e.getMessage());
		}


	}

	// mail with datasource
	public void sendHtmlMessageAttachment(Email email, DataSource ds) throws MessagingException, IOException {

		MimeMessage message = emailSender.createMimeMessage();

		message.reply(email.isReply());

		MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
				StandardCharsets.UTF_8.name());

		Context context = new Context();

		context.setVariables(email.getProperties());

		helper.setFrom(email.getFromEmail());
		helper.setTo(email.getToMail());
		helper.setSubject(email.getSubject());

		String html = templateEngine.process(email.getTemplate(), context);
		helper.setText(html, true);

		helper.addAttachment("booking_" + LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) + ".pdf", ds);

		log.info("Sending email: {} with html body: {}", email, html);
		try {
			emailSender.send(message);
			templateEngine.clearTemplateCache();
		} catch (Exception e) {
			System.out.println(e);
			System.out.println(e.getMessage());
			log.error("Error during email sending : {}", e.getMessage());
		}
	}

	private void saveEmail(Email email) {
		EmailEntity toSave = new EmailEntity();
		toSave.setFromMail(email.getFromEmail());
		toSave.setToMail(email.getToMail());
		toSave.setReply(email.isReply());
		toSave.setSubject(email.getSubject());
		toSave.setText(email.getText());
		toSave.setTemplate(email.getTemplate());

		toSave = emailRepository.save(toSave);

		log.info("Email {} with subject {} saved", toSave.getId(), toSave.getSubject());
	}
}